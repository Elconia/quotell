import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Constants from 'expo-constants';
import Axios from 'axios';
import ListItem from './ListItem';

class SearchScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: '',
      data: {}
    }
  }

  goToDetail(quoteID) {
    this.props.navigation.navigate('Detail', { quoteId: quoteID, height: '80%' });
  }

  fetchData = async () => {
    try {
      const response = await Axios.get(`https://favqs.com/api/quotes/?filter=${this.state.search}`, {
        headers: {
          Authorization: this.props.todo.appToken
        }
      });
      this.setState({ data: response.data });
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.logoImg} source={require('./image/logo.png')} />
          <Text style={styles.pBoldPink}>Search Quotes</Text>
        </View>

        <View style={{ flex: 1, padding: 24, paddingTop: 4, paddingBottom: 0 }}>
          <View style={styles.txtContainer}>
            <TextInput
              style={{ flex: 1 }}
              value={this.state.search}
              placeholder='Search quotes that fits you'
              placeholderTextColor='#FFC58B'
              onChangeText={search => this.setState({ search })} />
            <TouchableOpacity onPress={() => this.fetchData()} style={{ alignSelf: 'center', marginTop: -3 }}>
              <Icon style={{ color: '#FFC58B' }} name={'search'} size={25} />
            </TouchableOpacity>
          </View>

          <FlatList
            style={{ flex: 1, marginTop: 8 }}
            data={this.state.data.quotes}
            keyExtractor={(x, i) => String(i)}
            renderItem={({ item }) => <ListItem data={item} navigation={() => this.goToDetail(item.id)} />}
          />
        </View>

        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Constants.statusBarHeight
  },
  header: {
    height: 64,
    paddingVertical: 16,
    marginHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  logoImg: {
    height: 32,
    width: 95,
    resizeMode: 'contain'
  },
  pBoldPink: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FF83A1'
  },
  txtContainer: {
    width: '100%',
    height: 48,
    padding: 14,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#FFF6ED',
    borderRadius: 8
  },
});

const mapStateToProps = (state) => ({
  todo: state.todo
})

export default connect(mapStateToProps)(SearchScreen);