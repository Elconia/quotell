import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { connect } from 'react-redux';
import Constants from 'expo-constants';
import Swiper from 'react-native-swiper';
import Axios from 'axios';
import QuoteDetail from './QuoteDetail';

class SlideScreen extends Component {
  _isMounted = false;

  constructor(props) {
    super(props)
    this.state = {
      data: {}
    }
  }

  fetchData = async () => {
    try {
      const response = await Axios.get(`https://favqs.com/api/quotes`, {
        headers: {
          Authorization: this.props.todo.appToken
        }
      });
      this.setState({ data: response.data });
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this.fetchData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let slide = [], tinggi = '95%';

    if (this.state.data.quotes) {
      for (let quoteItem of this.state.data.quotes) {
        slide.push(
          <View key={quoteItem.id} style={styles.slide}>
            <QuoteDetail idQuote={quoteItem.id} height={tinggi} />
          </View>
        );
      }
    }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.logoImg} source={require('./image/logo.png')} />
          <Text style={styles.pBoldPink}>Random Quotes</Text>
        </View>

        <Swiper showsPagination={false} showsButtons={true}>
          {slide}
        </Swiper>

        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Constants.statusBarHeight
  },
  header: {
    height: 64,
    paddingVertical: 16,
    marginHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  logoImg: {
    height: 32,
    width: 95,
    resizeMode: 'contain'
  },
  pBoldPink: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FF83A1'
  },
  slide: {
    flex: 1,
    marginHorizontal: 32,
    marginTop: 24,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white'
  }
});

const mapStateToProps = (state) => ({
  todo: state.todo
})

export default connect(mapStateToProps)(SlideScreen);