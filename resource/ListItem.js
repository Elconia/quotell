import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class ListItem extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { data, navigation } = this.props;
    return (
      <TouchableOpacity onPress={navigation} style={{marginBottom: 16}}>
        <View style={styles.quoteListBox}>
          <Text style={styles.pBoldBlack}>{data.body}</Text>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 16 }}>
            <Text style={styles.pRegPink}>— {data.author}</Text>
          </View>
        </View>

        <View style={{flexDirection: 'row', justifyContent: 'space-between',}}>
          <View style={{ flexDirection: 'row' }}>
            <Icon name='heart-outline' color='#FF83A1' size={17} />
            <Text style={[styles.labelPink, { marginLeft: 4 }]}>{data.favorites_count}</Text>
          </View>

          <View style={{flexDirection: 'row'}}>
            <View style={styles.voteButton}>
              <Icon name='arrow-up-circle-outline' size={ 16 } color='#9D9D9D' style={{marginRight: 4}}/>
              <Text style={styles.labelGray}>{data.upvotes_count}</Text>
            </View>

            <View style={styles.voteButton}>
              <Icon name='arrow-down-circle-outline' size={ 16 } color='#9D9D9D' style={{marginRight: 4}}/>
              <Text style={styles.labelGray}>{data.downvotes_count}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  quoteListBox: {
    padding: 24,
    backgroundColor: '#FAF8F8',
    borderRadius: 8,
    marginBottom: 8,
  },

  voteButton: {
    flexDirection: 'row',
    alignItems: 'center',
    marginLeft: 8
  },

  // Font
  pBoldBlack: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#575757'
  },
  pRegPink: {
    fontSize: 16,
    color: '#FF83A1'
  },

  labelPink: {
    fontSize: 14,
    color: '#FF83A1'
  },

  labelGray: {
    fontSize: 14,
    color: '#9D9D9D'
  }
})