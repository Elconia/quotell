import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import Constants from 'expo-constants';

export default class AboutScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.logoImg} source={require('./image/logo.png')} />
          <Text style={styles.pBoldPink}>About Founders</Text>
        </View>

        {/* Fouders' profile */}
        <View style={{ flex: 1, justifyContent: 'center' }}>
          {/* Yo */}
          <View style={styles.founder}>
            <Image source={require('./image/yo.png')} style={styles.ppBig} />
            <Text style={styles.h1BoldBlack}>Yohanes Subagio</Text>
            <Text style={styles.pRegPink}>Hacker</Text>
          </View>

          {/* Target */}
          <View style={styles.founder}>
            <Image source={require('./image/get.png')} style={styles.ppBig} />
            <Text style={styles.h1BoldBlack}>Target Santana</Text>
            <Text style={styles.pRegPink}>Hipster</Text>
          </View>
        </View>

        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingTop: Constants.statusBarHeight
  },

  header: {
    height: 64,
    paddingVertical: 16,
    marginHorizontal: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  logoImg: {
    height: 32,
    width: 95,
    resizeMode: 'contain'
  },

  founder: {
    alignItems: 'center',
    marginVertical: 24
  },

  ppBig: {
    width: 96,
    height: 96
  },

  h1BoldBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#575757'
  },

  pBoldPink: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FF83A1'
  },

  pRegPink: {
    fontSize: 16,
    color: '#FF83A1'
  }
});