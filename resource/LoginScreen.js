import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, TextInput, Linking } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from 'expo-constants';
import Axios from 'axios';
import { CREATE_SESSION, ADD_USER } from './redux/constants/actionTypes';

class LoginScreen extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userName: '',
      password: '',
      isError: false,
      secureText: true,
      iconName: 'eye-off-outline'
    }
  }

  bootstrapAsync = async () => {
    try {
      const userToken = await AsyncStorage.getItem('userToken');
      const login = await AsyncStorage.getItem('login');
      const email = await AsyncStorage.getItem('email');

      if (userToken !== null) {
        let isSessionValid = false;
        let errorMessage = '';

        try {
          const response = await Axios.get(`https://favqs.com/api/users/${login}`, {
            headers: {
              Authorization: this.props.todo.appToken,
              'User-Token': userToken
            }
          });

          isSessionValid = true;
        } catch (error) {
          isSessionValid = false;
          errorMessage = error;
        }

        if (isSessionValid) {
          this.props.dispatch({
            type: CREATE_SESSION,
            payload: userToken
          });
          this.props.dispatch({
            type: ADD_USER,
            payload: [login, email]
          });
        } else {
          await AsyncStorage.removeItem('userToken');
          await AsyncStorage.removeItem('login');
          await AsyncStorage.removeItem('login');
        }
      }
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  };

  componentDidMount() {
    this.bootstrapAsync();
  }

  peekHandler() {
    if (this.state.secureText)
      this.setState({ secureText: false, iconName: 'eye-outline' });
    else
      this.setState({ secureText: true, iconName: 'eye-off-outline' });
  }

  loginHandler = async () => {
    try {
      const response = await Axios.post(`https://favqs.com/api/session`, {
        user: {
          login: this.state.userName,
          password: this.state.password
        }
      }, {
        headers: {
          Authorization: this.props.todo.appToken
        }
      });
      const data = response.data;
      if (data['User-Token']) {
        this.props.dispatch({
          type: CREATE_SESSION,
          payload: data['User-Token']
        });
        this.props.dispatch({
          type: ADD_USER,
          payload: [data.login, data.email]
        });

        await AsyncStorage.setItem('userToken', data['User-Token']);
        await AsyncStorage.setItem('login', data.login);
        await AsyncStorage.setItem('login', data.email);

        this.props.navigation.navigate('Home');
      } else {
        this.setState({ isError: true });
      }
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={{ marginTop: 16 }}>
          <Image style={styles.logoImg} source={require('./image/logo.png')} resizeMode={'contain'} />
        </View>
        <Text style={styles.hiText}>Hi, you are back!</Text>
        <Text style={styles.loginText}>Username or Email</Text>
        <View style={styles.txtContainer}>
          <TextInput
            style={{ flex: 1 }}
            value={this.state.userName}
            placeholder='Your username or email'
            placeholderTextColor='#FFC58B'
            onChangeText={userName => this.setState({ userName })} />
        </View>
        <Text style={styles.loginText}>Password</Text>
        <View style={styles.txtContainer}>
          <TextInput
            style={{ flex: 1 }}
            value={this.state.password}
            placeholder='Your secret word... ssshhh...'
            placeholderTextColor='#FFC58B'
            onChangeText={password => this.setState({ password })}
            secureTextEntry={this.state.secureText} />
          <TouchableOpacity onPress={() => this.peekHandler()} style={{ alignSelf: 'center', marginTop: -3 }}>
            <Icon style={{ color: '#FFC58B' }} name={this.state.iconName} size={25} />
          </TouchableOpacity>
        </View>
        <Text style={this.state.isError ? styles.errorText : styles.hiddenErrorText}>Wrong username or password</Text>
        <TouchableOpacity style={styles.signInButton} onPress={() => this.loginHandler()}>
          <Text style={styles.signInText}>Sign In</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ alignSelf: 'center' }}
          onPress={() => Linking.openURL('https://favqs.com/forgot-password')}>
          <Text style={styles.forgotPass}>Forgot password?</Text>
        </TouchableOpacity>
        <View style={{ flexDirection: 'row', width: '100%', justifyContent: 'center' }}>
          <Text style={styles.accountText}>Don't have an account? </Text>
          <TouchableOpacity onPress={() => Linking.openURL('https://favqs.com/login')}>
            <Text style={styles.signUp}>Sign Up</Text>
          </TouchableOpacity>
        </View>
        <StatusBar style="auto" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: '#fff',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    paddingTop: Constants.statusBarHeight
  },
  logoImg: {
    height: 48,
    width: 143,
  },
  hiText: {
    marginTop: 48,
    marginBottom: 12,
    color: '#FF83A1',
    fontWeight: 'bold',
    fontSize: 32
  },
  loginText: {
    marginTop: 12,
    marginBottom: 4,
    color: '#545454',
    fontWeight: 'bold',
    fontSize: 16
  },
  txtContainer: {
    width: '100%',
    height: 48,
    padding: 14,
    flexDirection: 'row',
    alignItems: 'flex-start',
    backgroundColor: '#FFF6ED',
    borderRadius: 8
  },
  errorText: {
    color: 'red',
    alignSelf: 'center',
    marginTop: 8,
    marginBottom: 8,
  },
  hiddenErrorText: {
    color: 'transparent',
    alignSelf: 'center',
    marginTop: 8,
    marginBottom: 8,
  },
  signInButton: {
    width: '100%',
    height: 48,
    padding: 14,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FF83A1',
    borderRadius: 8
  },
  signInText: {
    color: '#FFFFFF',
    fontWeight: 'bold',
    fontSize: 16
  },
  forgotPass: {
    margin: 16,
    color: '#65A8BD',
    fontSize: 16
  },
  accountText: {
    color: '#505050',
    fontSize: 16,
    alignSelf: 'center'
  },
  signUp: {
    color: '#65A8BD',
    fontSize: 16
  }
});

const mapStateToProps = (state) => ({
  todo: state.todo
})

export default connect(mapStateToProps)(LoginScreen);