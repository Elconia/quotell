import * as React from 'react';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import NavigationIndex from './NavigationIndex';

import { Provider } from 'react-redux';
import store from './redux/store/store'

export default () => {
  return (
    <Provider store={store()}>
      <NavigationContainer>
        <NavigationIndex />
      </NavigationContainer>
    </Provider>
  );
}