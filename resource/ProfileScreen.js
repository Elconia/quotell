import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-community/async-storage';
import Constants from 'expo-constants';
import Axios from 'axios';
import ListItem from './ListItem';
import { GET_USER, DESTROY_SESSION, ADD_FAV } from './redux/constants/actionTypes';

class ProfileScreen extends Component {
  _isMounted = false;

  constructor(props) {
    super(props)
  }

  goToDetail(quoteID) {
    this.props.navigation.navigate('Detail', { quoteId: quoteID, height: '80%' });
  }

  fetchData = async () => {
    try {
      const response = await Axios.get(`https://favqs.com/api/quotes/?filter=${this.props.todo.login}&type=user`, {
        headers: {
          Authorization: this.props.todo.appToken,
          'User-Token': this.props.todo.userToken
        }
      });
      this.props.dispatch({
        type: ADD_FAV,
        payload: response.data
      });
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  destroySession = async () => {
    try {
      await AsyncStorage.removeItem('userToken');
      await AsyncStorage.removeItem('login');
      await AsyncStorage.removeItem('login');

      this.props.dispatch({
        type: DESTROY_SESSION
      });
    } catch (error) {
      alert(`Destroy session failed!\n${error}`);
    }
  }

  getUser = async () => {
    try {
      const response = await Axios.get(`https://favqs.com/api/users/${this.props.todo.login}`, {
        headers: {
          Authorization: this.props.todo.appToken,
          'User-Token': this.props.todo.userToken
        }
      });
      const data = response.data;
      if (data['pic_url']) {
        this.props.dispatch({
          type: GET_USER,
          payload: { uri: data['pic_url'] }
        });
      }
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  componentDidMount() {
    this._isMounted = true;

    this.getUser();
    this.fetchData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    const { todo } = this.props;
    const image_url = todo.profilePic;
    return (
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.header}>
          <Image style={styles.logoImg} source={require('./image/logo.png')} />
          <Text style={styles.pBoldPink}>Profile</Text>
        </View>

        {/* Log Out Button */}
        <TouchableOpacity style={{ alignItems: 'flex-end' }} onPress={() => this.destroySession()}>
          <Text style={styles.labelRed}>Log Out</Text>
        </TouchableOpacity>

        {/* User Detail */}
        <View style={styles.userDetail}>
          <View style={styles.userDetailLeft}>
            <Image source={image_url} style={styles.ppMed} />

            {/* User Detail Text */}
            <View style={{ marginLeft: 12 }}>
              <Text style={styles.h1BoldBlack}>{todo.login}</Text>
              <View style={{ flexDirection: 'row', marginTop: 6, alignItems: 'center' }}>
                <Icon name='email-outline' color='#FF83A1' size={16} />
                <Text style={[styles.labelPink, { marginLeft: 4 }]}>{todo.email}</Text>
              </View>
            </View>
          </View>
        </View>

        {/* Fav Quotes */}
        <View style={styles.boxPink}>
          <Icon name='heart-outline' color='white' size={25} />
          <Text style={styles.pBoldWhite}>Favourite Quotes</Text>
        </View>

        {/* Favs List */}
        <FlatList
          data={this.props.todo.favoriteQuotes.quotes}
          keyExtractor={(x, i) => String(i)}
          renderItem={({ item }) => <ListItem data={item} navigation={() => this.goToDetail(item.id)} />}
        />

        <StatusBar style='auto' />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 24,
    paddingTop: Constants.statusBarHeight
  },

  header: {
    height: 64,
    paddingVertical: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  logoImg: {
    height: 32,
    width: 95,
    resizeMode: 'contain'
  },

  userDetail: {
    justifyContent: 'space-between',
    flexDirection: 'row',
    marginTop: 8
    // backgroundColor: 'red'
  },

  userDetailLeft: {
    flexDirection: 'row'
  },

  boxPink: {
    backgroundColor: '#FF83A1',
    width: 'auto',
    alignSelf: 'flex-start',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 8,
    marginVertical: 24
  },

  // Font Style
  h1BoldBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#575757'
  },

  pBoldPink: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FF83A1'
  },

  pBoldWhite: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },

  pRegPink: {
    fontSize: 16,
    color: '#FF83A1'
  },

  labelRed: {
    fontSize: 14,
    color: '#FF0000'
  },

  labelPink: {
    fontSize: 14,
    color: '#FF83A1'
  },

  // PP
  ppBig: {
    width: 96,
    height: 96
  },

  ppMed: {
    width: 64,
    height: 64,
    borderRadius: 100
  }
});

const mapStateToProps = (state) => ({
  todo: state.todo
})

export default connect(mapStateToProps)(ProfileScreen);