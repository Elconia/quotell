import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import LoginScreen from './LoginScreen';
import SlideScreen from './SlideScreen';
import SearchScreen from './SearchScreen';
import ProfileScreen from './ProfileScreen';
import AboutScreen from './AboutScreen';
import DetailScreen from './DetailScreen';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const HomeScreen = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ color, size }) => {
        let iconName;

        if (route.name === 'Slide') {
          iconName = 'vibrate';
        } else if (route.name === 'Search') {
          iconName = 'magnify';
        } else if (route.name === 'Profile') {
          iconName = 'account-outline';
        } else if (route.name === 'About') {
          iconName = 'star-outline';
        }

        // You can return any component that you like here!
        return <Icon name={iconName} size={size} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#FF83A1',
      inactiveTintColor: '#CCCCCC',
    }}
  >
    <Tab.Screen name="Slide" component={SlideScreen} />
    <Tab.Screen name="Search" component={SearchScreen} />
    <Tab.Screen name="Profile" component={ProfileScreen} />
    <Tab.Screen name="About" component={AboutScreen} />
  </Tab.Navigator>
);

class NavigationIndex extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      this.props.todo.userToken ? (
        <Stack.Navigator>
          <Stack.Screen name="Home" component={HomeScreen} options={{ headerShown: false }} />
          <Stack.Screen name="Detail" component={DetailScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      ) : (
        <Stack.Navigator>
          <Stack.Screen name="Login" component={LoginScreen} options={{ headerShown: false }} />
        </Stack.Navigator>
      )
    );
  }
}

const mapStateToProps = (state) => ({
  todo: state.todo
})

export default connect(mapStateToProps)(NavigationIndex);