import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Constants from 'expo-constants';
import QuoteDetail from './QuoteDetail';

export default class DetailScreen extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { quoteId, height } = this.props.route.params;
    return (
      <View style={styles.container}>
        {/* Header */}
        <View style={styles.header}>
          <TouchableOpacity
            style={{ height: 25 }}
            onPress={() => this.props.navigation.goBack()}>
            <Icon name='chevron-left' size={25} color='#FF83A1' />
          </TouchableOpacity>

          <Text style={styles.pBoldPink}>Quote Detail</Text>
        </View>

        <QuoteDetail idQuote={quoteId} height={height} />

        <StatusBar style='auto' />
      </View>
    )
  }
}

// <QuoteDetail quoteId={quoteId} />

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginHorizontal: 24,
    paddingTop: Constants.statusBarHeight
  },

  header: {
    height: 64,
    paddingVertical: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },

  pBoldPink: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FF83A1'
  }
});