import { CREATE_SESSION, DESTROY_SESSION, ADD_USER, GET_USER, ADD_FAV } from '../constants/actionTypes'

const initialState = {
  appToken: `Token token="058be5bf24f6e9a6362d2a20112fb634"`,
  userToken: null,
  login: null,
  email: null,
  profilePic: require('../../image/userpp.png'),
  favoriteQuotes: {}
};

const todoReducer = (state = initialState, action) => {
  switch (action.type) {
    case CREATE_SESSION:
      return {
        ...state,
        userToken: action.payload
      }
    case DESTROY_SESSION:
      return {
        ...state,
        userToken: null,
        login: null,
        email: null,
        profilePic: require('../../image/userpp.png')
      }
    case ADD_USER:
      return {
        ...state,
        login: action.payload[0],
        email: action.payload[1],
      }
    case GET_USER:
      return {
        ...state,
        profilePic: action.payload
      }
    case ADD_FAV:
      return {
        ...state,
        favoriteQuotes: action.payload
      }
    default:
      return state;
  }
}

export default todoReducer;