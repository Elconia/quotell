import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';
import { ADD_FAV } from './redux/constants/actionTypes';
import { set } from 'react-native-reanimated';

class QuoteDetail extends Component {
  _isMounted = false;

  constructor(props) {
    super(props)
    this.state = {
      details: {},
      tags: [],
      data: {},
      height: '80%'
    };
  }

  updateFav = async () => {
    try {
      const response = await Axios.get(`https://favqs.com/api/quotes/?filter=${this.props.todo.login}&type=user`, {
        headers: {
          Authorization: this.props.todo.appToken,
          'User-Token': this.props.todo.userToken
        }
      });
      this.props.dispatch({
        type: ADD_FAV,
        payload: response.data
      });
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  fetchData = async () => {
    try {
      const response = await Axios.get(`https://favqs.com/api/quotes/${this.props.idQuote}`, {
        headers: {
          Authorization: this.props.todo.appToken,
          'User-Token': this.props.todo.userToken
        }
      });
      this.setState({ details: response.data.user_details });
      this.setState({ tags: response.data.tags });
      this.setState({ data: response.data });
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  toggleFav = async (quoteId, favorite) => {
    try {
      if (favorite) {
        const response = await Axios.put(`https://favqs.com/api/quotes/${quoteId}/unfav`, {}, {
          headers: {
            Authorization: this.props.todo.appToken,
            'User-Token': this.props.todo.userToken
          }
        });
        alert(`You remove this Quote from your Favorite Quotes`);
      } else {
        const response = await Axios.put(`https://favqs.com/api/quotes/${quoteId}/fav`, {}, {
          headers: {
            Authorization: this.props.todo.appToken,
            'User-Token': this.props.todo.userToken
          }
        });
        alert(`You add this Quote to your Favorite Quotes`);
      }
      this.updateFav();
      this.fetchData(this.props.quoteId);
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  toggleUpvote = async (quoteId, upvote) => {
    try {
      if (!upvote) {
        const response = await Axios.put(`https://favqs.com/api/quotes/${quoteId}/upvote`, {}, {
          headers: {
            Authorization: this.props.todo.appToken,
            'User-Token': this.props.todo.userToken
          }
        });
      } else {
        const response = await Axios.put(`https://favqs.com/api/quotes/${quoteId}/clearvote`, {}, {
          headers: {
            Authorization: this.props.todo.appToken,
            'User-Token': this.props.todo.userToken
          }
        });
      }
      this.fetchData(this.props.quoteId);
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  toggleDownvote = async (quoteId, downvote) => {
    try {
      if (!downvote) {
        const response = await Axios.put(`https://favqs.com/api/quotes/${quoteId}/downvote`, {}, {
          headers: {
            Authorization: this.props.todo.appToken,
            'User-Token': this.props.todo.userToken
          }
        });
      } else {
        const response = await Axios.put(`https://favqs.com/api/quotes/${quoteId}/clearvote`, {}, {
          headers: {
            Authorization: this.props.todo.appToken,
            'User-Token': this.props.todo.userToken
          }
        });
      }
      this.fetchData(this.props.quoteId);
    } catch (error) {
      alert(`Fetching data failed!\n${error}`);
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this.setState({ height: this.props.height });
    this.fetchData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    let userDetails = {}, tagView = [], i = 0;
    const tags = JSON.stringify(this.state.tags);
    const tagsJSON = JSON.parse(tags);

    for (let tagItem of tagsJSON) {
      tagView.push(
        <TouchableOpacity key={i}><Text style={[styles.labelWhite, { marginRight: 4 }]}>#{tagItem}</Text></TouchableOpacity>
      );
      i++;
    }

    const stringUser = JSON.stringify(this.state.details);
    userDetails = JSON.parse(stringUser);

    return (
      <View style={{ ...styles.quoteDetailBox, ...{ height: this.state.height } }}>
        {/* Heading */}
        <View style={styles.quoteDetailBoxHeading}>
          <Text style={styles.pRegWhite}>Quote by <Text style={styles.pBoldWhite}>{this.state.data.author}</Text></Text>

          <TouchableOpacity onPress={() => this.toggleFav(this.state.data.id, userDetails.favorite)}>
            <Icon name={userDetails.favorite ? 'heart' : 'heart-outline'} size={24} color='white' />
          </TouchableOpacity>
        </View>

        {/* Quote Details Container*/}
        <View style={styles.quoteDetails}>
          <Text style={[styles.h1BoldWhite, { textAlign: 'center' }]}>“{this.state.data.body}”</Text>
          <Text style={[styles.h1RegWhite, { textAlign: 'center' }]}>- {this.state.data.author}</Text>
          <Text style={[styles.pRegWhite, { textAlign: 'center' }]}>{this.state.data.context}</Text>
        </View>

        {/* Detail Props */}
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <View style={{ width: '80%', flexWrap: 'wrap', flexDirection: 'row', alignSelf: 'flex-end' }}>
            {tagView}
          </View>

          <View>
            <TouchableOpacity
              onPress={() => this.toggleUpvote(this.state.data.id, userDetails.upvote)}
              style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 4 }}>
              <Text style={[styles.labelWhite, { marginRight: 8 }]}>{this.state.data.upvotes_count}</Text>
              <Icon
                style={userDetails.upvote ? { opacity: 1 } : { opacity: .5 }}
                name='arrow-up-circle-outline'
                size={25}
                color='white' />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => this.toggleDownvote(this.state.data.id, userDetails.downvote)}
              style={{ flexDirection: 'row', alignItems: 'center', marginVertical: 4 }}>
              <Text style={[styles.labelWhite, { marginRight: 8 }]}>{this.state.data.downvotes_count}</Text>
              <Icon
                style={userDetails.downvote ? { opacity: 1 } : { opacity: .5 }}
                name='arrow-down-circle-outline'
                size={25}
                color='white' />
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  quoteDetailBox: {
    backgroundColor: '#B796FF',
    borderRadius: 24,
    padding: 24,
    justifyContent: 'space-between'
  },

  quoteDetailBoxHeading: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },

  quoteDetails: {
    // backgroundColor: 'red',
    height: '70%',
    justifyContent: 'space-around'
  },

  // Font Style
  h1BoldBlack: {
    fontSize: 24,
    fontWeight: 'bold',
    color: '#575757'
  },

  h1BoldWhite: {
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white'
  },

  h1RegWhite: {
    fontSize: 24,
    color: 'white'
  },

  pBoldBlack: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#575757'
  },

  pBoldWhite: {
    fontSize: 16,
    fontWeight: 'bold',
    color: 'white'
  },

  pRegPink: {
    fontSize: 16,
    color: '#FF83A1'
  },

  pBoldWhite: {
    fontSize: 16,
    color: 'white',
    fontWeight: 'bold'
  },

  pRegWhite: {
    fontSize: 16,
    color: 'white'
  },

  pRegRed: {
    fontSize: 16,
    color: '#FF0000'
  },

  labelWhite: {
    fontSize: 14,
    color: 'white'
  }
});

const mapStateToProps = (state) => ({
  todo: state.todo
})

export default connect(mapStateToProps)(QuoteDetail);